package ExceptionHandling;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class ExceptionCatcher {
    public static void main(String[] args) {
        int num = 0;
        try{
            System.out.println("Catching new Error");
            ExceptionCatcher.getValue(num);
        }
        catch(IllegalArgumentException  e)
        {
            Logger.getLogger(ExceptionCatcher.class.getName()).log(new LogRecord(Level.WARNING, " ======== num < 0 or num > 5 ==========="));
            throw new IllegalArgumentException(e);
        }
    }
    public static int getValue(int num)
    {
        System.out.println("Input number (< 0 or > 5 will cast an error)");
        Scanner in = new Scanner(System.in);
        num = in.nextInt();
        if(num < 0 || num > 5) throw new IllegalArgumentException ("ExpectionCatched");
        return num;
    }
}
