package airCompany;

public class InitCargoPlanes {
}
class AirbusA300 extends CargoPlane {
    AirbusA300 () {
        super(2,0.078, 68149, 55017, 7540);
    }
    public static void fly(){
        System.out.println("AirbusA300 flying");
    }
}
class Boeing767F extends CargoPlane {
    Boeing767F() {
        super(2, 0.068, 90774, 44497, 7130);
    }
    public static void fly() {
        System.out.println("Boeing767F flying");
    }
}
class BoeingDreamLifter extends CargoPlane {
    BoeingDreamLifter() {
        super(2, 0.039, 199146, 113400, 7800);
    }
    public static void fly() {
        System.out.println("BoeingDreamLifter flying");
    }
}
class An124 extends CargoPlane {
    An124() {
        super(6, 0.044, 348740,214000, 7600);
    }
    public static void fly() {
        System.out.println("An124 flying");
    }
}