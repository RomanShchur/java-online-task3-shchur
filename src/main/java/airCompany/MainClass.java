package airCompany;
import java.util.Scanner;
import java.lang.*;
import java.lang.reflect.*;
import java.awt.*;

enum MenuItems{
    CountCapacities,
    SortPlanes,
    SearchPlane,
    Exit,
    Continue
}
public class MainClass {
    public static void main(String[] args) {
        
        menuIn();
        System.out.println();
        
    }
    public static void menuOut(){
        System.out.println("\tCountCapacities.\tCount passenger and bearing capacities");
        System.out.println("\tSortPlanes.\tSort planes by flying range");
        System.out.println("\tSearchPlane.\tSearch plane by flying range");
        System.out.println("\tExit.\tExit");
        System.out.print("\nSelected option number -> ");
    }
    public static void menuIn() {
        Scanner inp = new Scanner(System.in);
        MenuItems menuItems = MenuItems.Continue;
        while (menuItems != MenuItems.Exit)
            try
            {
                menuOut();
                menuItems = MenuItems.valueOf(inp.nextLine());
                switch (menuItems)
                {
                    case CountCapacities: {
                        System.out.println("Counting capacities");
                        int pC01s = 0;
                        int bC01s = 0;
                        countPassengerCapacity(pC01s);
                        countBearingCapacity(bC01s);
                        System.out.println(" Passenger cap "+ pC01s +" Cargo cap "+ bC01s);
                        break;
                    }
                    case SortPlanes: {
                        System.out.println("Still under construction.");
                        break;
                    }
                    case SearchPlane: {
                        System.out.println("Plane Search");
                        searchPlaneBy();
                        break;
                    }
                    case Exit:
                        System.out.println("Program ends.");
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Selection out of range.");
                }
            }
            catch (IllegalArgumentException e)
            {
                System.out.println("Selection out of range. Try again:");
            }
    }
    public static int countPassengerCapacity(int pC01){
        Airliner Boeing777 = new Airliner(10, 0.078, 181283, 451, 13936);
        pC01 = Boeing777.passengerCapacity;
        return pC01;
    }
    public static int countBearingCapacity(int bC01){
        CargoPlane AirbusA300 = new CargoPlane(10, 0.078, 181283, 451, 13936);
        bC01 = AirbusA300.bearingCapacity;
        return bC01;
    }
    public static Plane searchPlaneBy(){
        Airliner Boeing777_01 = new Airliner(10, 0.078, 181283, 451, 13936);
        Airliner AirbusA350_01 = new Airliner(10, 0.103, 150000, 350,15600 );
        CargoPlane AirbusA300_01 = new CargoPlane(2,0.078, 68149, 55017, 7540);
        CargoPlane BoeingDreamLifter_01 = new CargoPlane(2, 0.039, 199146, 113400, 7800);

        Scanner inp = new Scanner(System.in);
        System.out.println("Input Flying range");
        int inRange = inp.nextInt();
        if (inRange == Boeing777_01.rangeOfFlight) {
            System.out.println("Range of Boeing777");
            return Boeing777_01;
        }
        else if (inRange == AirbusA350_01.rangeOfFlight){
            System.out.println("Range of AirbusA350");
            return AirbusA350_01;
        }
        else if (inRange == AirbusA300_01.rangeOfFlight){
            System.out.println("Range of AirbusA300");
            return AirbusA300_01;
        }
        else if (inRange == BoeingDreamLifter_01.rangeOfFlight){
            System.out.println("Range of BoeingDreamLifter");
            return BoeingDreamLifter_01;
        }
        else {
            System.out.println("There are no planes with range like this.");
            return null;
        }
    }
}
