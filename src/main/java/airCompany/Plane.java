package airCompany;

/**
 * Class Plane
 * Contains fields:
 * int crew (needed crew as pilots, engineers...)
 * fuelSupply (Liters) (fuel caring)
 * fuelConsumption (km/liter) (fuel consumes for 100 km)
 * rangeOfFlight(km)
 */
class Plane {
    String serialNumber;
    int crew;
    double fuelSupply;
    double fuelConsumption;
    int rangeOfFlight;
}

/**
 * Class CargoPlane
 * contains super fields +
 * int bearingCapacity (Kg)
 */
class CargoPlane extends Plane{
    int bearingCapacity;
     CargoPlane(int c, double fS, double fC, int bC, int rOf){
        int crew = c;
        double fuelSupply = fS;
        double fuelConsumption = fC;
        int bearingCapacity = bC;
        int rangeOfFlight = rOf;
    }
}
/**
 * Class Airliner
 * contains super fields +
 * int passengerCapacity (passengers(in 1'st class))
 */
class Airliner extends Plane{
    int passengerCapacity;
    Airliner(int c, double fS, double fC, int pC, int rOf){
        int crew = c;
        double fuelSupply = fS;
        double fuelConsumption = fC;
        int passengerCapacity = pC;
        int rangeOfFlight = rOf;
    }
}

